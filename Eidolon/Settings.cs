﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoEidolon
{
    public class Settings
    {
        public bool Eidolon1 { get; set; }
        public bool Eidolon2 { get; set; }
        public bool Eidolon3 { get; set; }
        public bool Eidolon4 { get; set; }
        public int Time { get; set; }
        public Keys HotKey { get; set; }

        public Settings()
        {
            Eidolon1 = false;
            Eidolon2 = false;
            Eidolon3 = false;
            Eidolon4 = false;
            Time = 8;
            HotKey = Keys.F1;
        }

    }
}
