﻿using System;
using System.Collections.Generic;
using Plugins;
using PluginsCommon;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Threading;
using System.Reflection;
using System.Runtime.InteropServices;

namespace AutoEidolon
{
    public class Main : IPlugin
    {

        [DllImport("User32.dll")]
        private static extern short GetAsyncKeyState(System.Windows.Forms.Keys vKey);
        [DllImport("User32.dll")]
        private static extern short GetAsyncKeyState(System.Int32 vKey);

        public static LocalPlayer Me { get; set; }
        public static Settings Settings;
    //    public static string path = Directory.GetParent(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).ToString().Remove(0, 6)) + @"\profiles\plugins\AutoEidolon\";
        public static string XMLname = "AutoEidolon.xml";
        public static string fullPath = Helper.ProfilesDirectory + XMLname;
        private static readonly Stopwatch timer = new Stopwatch();
        private static List<int> eidolonList = new List<int>();
        private static short eidolonNumber { get; set; } // integer to keep track of current eidolon
        private static int cooldown { get; set; } // in seconds
        private static Gui _gui;
        private Thread _guiManager;
        public static bool pluginRunning = true;
        public static string LoadedFile { get; set; }

        public string Author
        {
            get
            {
                return "Liz";
            }
        }

        public string Description
        {
            get
            {
                return "Automatically summon eidolons";
            }
        }

        public string Name
        {
            get
            {
                return "AutoEidolon";
            }
        }

        public Version Version
        {
            get { return Assembly.GetExecutingAssembly().GetName().Version; }
        }

        private void _guiManagement()
        {
            if (_gui == null)
            {
                _gui = new Gui();
            }
            _gui.ShowDialog();
        }
        public void OnButtonClick()
        {
            if (_guiManager == null ||
                _guiManager.ThreadState == System.Threading.ThreadState.Unstarted ||
                _guiManager.ThreadState == System.Threading.ThreadState.Aborted ||
                _guiManager.ThreadState == System.Threading.ThreadState.Stopped)
            {
                _guiManager = new Thread(_guiManagement);
                _guiManager.SetApartmentState(ApartmentState.STA);
                _guiManager.Start();
            }
            else
            {
                if (_gui != null)
                {
                    _gui.Invoke((MethodInvoker)delegate
                    {
                        _gui.Close();
                    });
                }
            }
        }

        public void OnStart()
        {
            try
            {
                Skandia.Update();
                if (!Directory.Exists(Helper.ProfilesDirectory))
                {
                    Directory.CreateDirectory(Helper.ProfilesDirectory);
                }

                // Create the profile directory for saving settings if it doesn't exist
                if (!Directory.Exists(Helper.ProfilesDirectory))
                    Directory.CreateDirectory(Helper.ProfilesDirectory);

                Skandia.MessageLog("AutoEidolon: Started");
                // Create gui to set up the settings
                if (_gui == null)
                    _gui = new Gui();
                

                // Set up the variables from the settings
                try
                {
                    string FILENAME = Path.Combine(Helper.ProfilesDirectory, Skandia.Me.Name + ".xml");
                    _gui.initialLoad(FILENAME);
                }
                catch
                {
                    Settings = new Settings();
                }
                updateSettings();
            }
            catch (Exception e)
            {
            }
        }

        public static void updateSettings()
        {
            try
            {
                // Set the timer
                if (Settings.Time < 1 || String.IsNullOrEmpty(Settings.Time.ToString()))
                {
                    cooldown = 8;
                }
                else
                    cooldown = Settings.Time;

                // Clear the eidolon list
                eidolonList.Clear();

                // Fill the eidolon list
                if (Settings.Eidolon1)
                    eidolonList.Add(1);
                if (Settings.Eidolon2)
                    eidolonList.Add(2);
                if (Settings.Eidolon3)
                    eidolonList.Add(3);
                if (Settings.Eidolon4)
                    eidolonList.Add(4);

                // Sort the list
                eidolonList.Sort();

                
            }
            catch (Exception e)
            {
                // IGNORE
            }
        }

        public void OnStop(bool off)
        {
            try
            {
                if (_gui != null)
                {
                    _gui.Invoke((MethodInvoker)delegate
                    {
                        _gui.Close();
                    });
                }
                else
                {
                    if (_gui != null && !_gui.IsDisposed)
                    {
                        _gui.Close();
                    }
                }
            }
            catch (Exception)
            {
                // IGNORE
            }
            if (_guiManager != null)
            {
                _guiManager.Abort();
            }
            timer.Stop();
        }

        public void Pulse()
        {
            try
            {
                short key = GetAsyncKeyState(Settings.HotKey);
                bool curPressed = ((key & 0x1) != 0);
                if (curPressed == true)
                {
                    if (pluginRunning)
                        pluginRunning = false;
                    else
                        pluginRunning = true;
                }

                _gui.UpdateStatus();

                if (!pluginRunning || ObjectManager.GetCurrentMapInfo().Type != MapType.Dungeon)
                    return;
                
                // Security checks
                Skandia.Update();
                Me = Skandia.Me;
                if (!Skandia.IsInGame || Me == null)
                    return;

                if (Settings == null || eidolonList.Count < 1)
                    return;

                // Start timer if not running
                if (!timer.IsRunning)
                    timer.Start();

                // If time has not elapsed - return
                if (timer.ElapsedMilliseconds < cooldown * 1000)
                    return;

                // Reset the counter
                if (String.IsNullOrEmpty(eidolonNumber.ToString()) || eidolonNumber >= eidolonList.Count)
                    eidolonNumber = 0;

                // Summon eidolon
                Me.ToggleEidolonBySlot((short)(eidolonList[eidolonNumber]-1));
                //MessageBox.Show("Eidolon No." + eidolonList[eidolonNumber].ToString() + Environment.NewLine + "eidolonNumber " + eidolonNumber.ToString());

                // Increment the counter
                eidolonNumber++;

                
                timer.Restart();
            }
            catch (Exception e)
            {
                //MessageBox.Show("ERROR:" + Environment.NewLine + e.Data);
            }
        }
    }
}
