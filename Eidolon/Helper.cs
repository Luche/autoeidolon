﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace AutoEidolon
{
    public class Helper
    {
        public static string ProfilesDirectory
        {
            get
            {
                var codeBase = Assembly.GetExecutingAssembly().CodeBase;
                var uri = new UriBuilder(codeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                // ReSharper disable once AssignNullToNotNullAttribute
                return Path.Combine(Directory.GetParent(Path.GetDirectoryName(path)).FullName, "profiles", "plugins", "AutoEidolon");
            }
        }
        public static T Load<T>(string file)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var reader = XmlReader.Create(Path.Combine(ProfilesDirectory, file)))
            {
                return (T)serializer.Deserialize(reader);
            }
        }
        public static void Save<T>(string file, T instance)
        {
            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            var serializer = new XmlSerializer(typeof(T));
            using (var writer = new StreamWriter(Path.Combine(ProfilesDirectory, file)))
            {
                using (var xmlWriter = XmlWriter.Create(writer, new XmlWriterSettings { OmitXmlDeclaration = true, Indent = true, IndentChars = "\t" }))
                {
                    serializer.Serialize(xmlWriter, instance, ns);
                }
            }
        }
    }
}
