﻿namespace AutoEidolon
{
    partial class Gui
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.eido1 = new System.Windows.Forms.CheckBox();
            this.eido2 = new System.Windows.Forms.CheckBox();
            this.eido3 = new System.Windows.Forms.CheckBox();
            this.eido4 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timeBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.hotkeyBox = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // eido1
            // 
            this.eido1.AutoSize = true;
            this.eido1.Location = new System.Drawing.Point(7, 8);
            this.eido1.Name = "eido1";
            this.eido1.Size = new System.Drawing.Size(70, 17);
            this.eido1.TabIndex = 0;
            this.eido1.Text = "Eidolon 1";
            this.eido1.UseVisualStyleBackColor = true;
            this.eido1.CheckedChanged += new System.EventHandler(this.eido1_CheckedChanged);
            // 
            // eido2
            // 
            this.eido2.AutoSize = true;
            this.eido2.Location = new System.Drawing.Point(7, 32);
            this.eido2.Name = "eido2";
            this.eido2.Size = new System.Drawing.Size(70, 17);
            this.eido2.TabIndex = 1;
            this.eido2.Text = "Eidolon 2";
            this.eido2.UseVisualStyleBackColor = true;
            this.eido2.CheckedChanged += new System.EventHandler(this.eido2_CheckedChanged);
            // 
            // eido3
            // 
            this.eido3.AutoSize = true;
            this.eido3.Location = new System.Drawing.Point(108, 8);
            this.eido3.Name = "eido3";
            this.eido3.Size = new System.Drawing.Size(70, 17);
            this.eido3.TabIndex = 2;
            this.eido3.Text = "Eidolon 3";
            this.eido3.UseVisualStyleBackColor = true;
            this.eido3.CheckedChanged += new System.EventHandler(this.eido3_CheckedChanged);
            // 
            // eido4
            // 
            this.eido4.AutoSize = true;
            this.eido4.Location = new System.Drawing.Point(108, 31);
            this.eido4.Name = "eido4";
            this.eido4.Size = new System.Drawing.Size(70, 17);
            this.eido4.TabIndex = 3;
            this.eido4.Text = "Eidolon 4";
            this.eido4.UseVisualStyleBackColor = true;
            this.eido4.CheckedChanged += new System.EventHandler(this.eido4_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Spawn delay";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Hotkey";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // timeBox
            // 
            this.timeBox.Location = new System.Drawing.Point(108, 73);
            this.timeBox.Name = "timeBox";
            this.timeBox.Size = new System.Drawing.Size(31, 20);
            this.timeBox.TabIndex = 6;
            this.timeBox.Text = "8";
            this.timeBox.TextChanged += new System.EventHandler(this.timeBox_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(145, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(12, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "s";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(103, 154);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(7, 154);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "Load";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // hotkeyBox
            // 
            this.hotkeyBox.Location = new System.Drawing.Point(103, 114);
            this.hotkeyBox.Name = "hotkeyBox";
            this.hotkeyBox.Size = new System.Drawing.Size(75, 23);
            this.hotkeyBox.TabIndex = 11;
            this.hotkeyBox.Text = "Click to edit";
            this.hotkeyBox.UseVisualStyleBackColor = true;
            this.hotkeyBox.Click += new System.EventHandler(this.hotkeyBox_Click_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 189);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 20);
            this.label4.TabIndex = 13;
            this.label4.Text = "Status";
            // 
            // Gui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(192, 218);
            this.Controls.Add(this.eido4);
            this.Controls.Add(this.eido1);
            this.Controls.Add(this.hotkeyBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.eido2);
            this.Controls.Add(this.timeBox);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.eido3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Name = "Gui";
            this.Text = "Gui";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox eido1;
        private System.Windows.Forms.CheckBox eido2;
        private System.Windows.Forms.CheckBox eido3;
        private System.Windows.Forms.CheckBox eido4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox timeBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button hotkeyBox;
        private System.Windows.Forms.Label label4;
    }
}