﻿using Plugins;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace AutoEidolon
{
    public partial class Gui : Form
    {
        public Settings settings = new Settings();
        private string fullPath = Main.fullPath;
        private bool hotkeyBoxClick = true;

        public Gui()
        {
            InitializeComponent();
            timeBox.LostFocus += TimeBox_LostFocus;
            hotkeyBox.Click += HotkeyBox_Click;
        }

        private void HotkeyBox_Click(object sender, EventArgs e)
        {
            try
            {
                if (hotkeyBoxClick)
                {
                    hotkeyBox.Text = "Press key...";
                    hotkeyBox.KeyDown += HotkeyBox_KeyDown;

                }
            }
            catch
            {

            }
        }

        private void HotkeyBox_KeyDown(object sender, KeyEventArgs e)
        {
            hotkeyBox.Text = e.KeyCode.ToString();
            Main.Settings.HotKey = e.KeyCode;
            hotkeyBox.KeyDown -= HotkeyBox_KeyDown;

            if (Main.pluginRunning)
                Main.pluginRunning = false;
            else
                Main.pluginRunning = true;
        }

        private void TimeBox_LostFocus(object sender, EventArgs e)
        {
            try
            {
                Main.Settings.Time = Convert.ToInt32(timeBox.Text);
                Main.updateSettings();
            }
            catch
            {
                timeBox.Text = "8";
                Main.Settings.Time = 8;
                Main.updateSettings();
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = true;
            Hide();
            base.OnClosing(e);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Open AutoEidolon Setting File";
            openFileDialog1.Filter = "XML files|*.xml";
            openFileDialog1.InitialDirectory = Helper.ProfilesDirectory;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Main.LoadedFile = openFileDialog1.FileName;

                if (File.Exists(Main.LoadedFile))
                {
                    Main.Settings = Helper.Load<Settings>(Main.LoadedFile);
                    Skandia.MessageLog("Loaded Profile: " + Main.LoadedFile);

                    // Create the UI according to the settings
                    eido1.Checked = Main.Settings.Eidolon1;
                    eido2.Checked = Main.Settings.Eidolon2;
                    eido3.Checked = Main.Settings.Eidolon3;
                    eido4.Checked = Main.Settings.Eidolon4;
                    timeBox.Text = Main.Settings.Time.ToString();
                    hotkeyBox.Text = Main.Settings.HotKey.ToString();
                    Main.updateSettings();
                }
                else
                {
                    Skandia.MessageLog("No Profile to load has been found.");
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Title = "Save Setting File";
            saveFileDialog1.Filter = "XML files|*.XML";
            saveFileDialog1.InitialDirectory = Helper.ProfilesDirectory;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {              
                Helper.Save(saveFileDialog1.FileName, Main.Settings);
            }
        }

        private void eido1_CheckedChanged(object sender, EventArgs e)
        {
            if (eido1.CheckState == CheckState.Checked)
            {
                Main.Settings.Eidolon1 = true;
            }
            else
            {
                Main.Settings.Eidolon1 = false;
            }
            Main.updateSettings();
        }

        private void eido3_CheckedChanged(object sender, EventArgs e)
        {

            if (eido3.CheckState == CheckState.Checked)
            {
                Main.Settings.Eidolon3 = true;
            }
            else
            {
                Main.Settings.Eidolon3 = false;
            }
            Main.updateSettings();
        }

        private void eido2_CheckedChanged(object sender, EventArgs e)
        {

            if (eido2.CheckState == CheckState.Checked)
            {
                Main.Settings.Eidolon2 = true;
            }
            else
            {
                Main.Settings.Eidolon2 = false;
            }
            Main.updateSettings();
        }

        private void eido4_CheckedChanged(object sender, EventArgs e)
        {

            if (eido4.CheckState == CheckState.Checked)
            {
                Main.Settings.Eidolon4 = true;
            }
            else
            {
                Main.Settings.Eidolon4 = false;
            }
            Main.updateSettings();
        }

        private void timeBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Main.Settings.Time = Convert.ToInt32(timeBox.Text);
                Main.updateSettings();
            }
            catch
            {
            }
        }

        private void hotkeyBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                KeysConverter convertKeys = new KeysConverter();
                Main.Settings.HotKey = (Keys)convertKeys.ConvertFromInvariantString(hotkeyBox.Text);
                Main.updateSettings();
            }
            catch
            {

            }
        }

        private void hotkeyBox_Click_1(object sender, EventArgs e)
        {

        }

        public void UpdateStatus()
        {
            if (Main.pluginRunning)
            {
                label4.Text = "The plugin is running";
                label4.ForeColor = Color.Green;
            }
            else
            {
                label4.Text = "The plugin is stopped";
                label4.ForeColor = Color.Red;
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        public void initialLoad(string filename)
        {
            if (File.Exists(filename))
            {
                Main.LoadedFile = filename;
                Main.Settings = Helper.Load<Settings>(Main.LoadedFile);
                Skandia.MessageLog("Loaded Profile: " + Main.LoadedFile);

                // Create the UI according to the settings
                eido1.Checked = Main.Settings.Eidolon1;
                eido2.Checked = Main.Settings.Eidolon2;
                eido3.Checked = Main.Settings.Eidolon3;
                eido4.Checked = Main.Settings.Eidolon4;
                timeBox.Text = Main.Settings.Time.ToString();
                hotkeyBox.Text = Main.Settings.HotKey.ToString();
                Main.updateSettings();
            }
            else
            {
                Skandia.MessageLog("No Profile to load has been found.");
                throw new FileLoadException();
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
